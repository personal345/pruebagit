/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author diegoh
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Auto autito = new Auto(4, "rojo", 3);
        Auto unAuto = new Auto(1, "verde", 3);
        Auto otroAuto = new Auto(4, "azul", 0);
        System.out.println(unAuto.toString());
        System.out.println(otroAuto.toString());
        System.out.println(autito.toString());
    }
    
}
