/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author diegoh
 */
public class Auto {

    private int puertas;
    private String color;
    private int ruedas;

    public Auto(int puertas, String color, int ruedas) {
        this.puertas = puertas;
        this.color = color;
        this.ruedas = ruedas;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getRuedas() {
        return ruedas;
    }

    public void setRuedas(int ruedas) {
        this.ruedas = ruedas;
    }

    @Override
    public String toString() {
        return "Mi autito es de " + "color=" + color + ", tiene ruedas=" + ruedas + " y tiene puertas=" + puertas + '.';
    }
}
